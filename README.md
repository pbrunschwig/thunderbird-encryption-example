About
=====

This is an example addon for Thunderbird that demonstrates how to implement
encryption and decryption in an add-on.

The addon is running at least with Thunderbird versions 52 to 64.

The addon implements simple encryption and decryption, using base64-encoding for the purpose of the demo,
for a new Content-Type `multipart/thunderbird-sample`.
