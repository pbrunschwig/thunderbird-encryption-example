/*global Components: false */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

"use strict";


/* global APP_SHUTDOWN: false */
const Cu = Components.utils;
const Cc = Components.classes;
const Ci = Components.interfaces;


function install() {}

function uninstall() {}

function startup(data, reason) {
  const SampleMimeEncrypt = Cu.import("chrome://sample/content/modules/mimeEncrypt.jsm").SampleMimeEncrypt;
  const SampleMimeDecrypt = Cu.import("chrome://sample/content/modules/mimeDecrypt.jsm").SampleMimeDecrypt;
  const SampleOverlays = Cu.import("chrome://sample/content/modules/sampleOverlays.jsm").SampleOverlays;

  try {
    SampleMimeEncrypt.startup(reason);
    SampleMimeDecrypt.startup(reason);
    SampleOverlays.startup(reason);
  }
  catch (ex) {
    logException(ex);
  }
}

function shutdown(data, reason) {
  try {
    const SampleMimeDecrypt = Cu.import("chrome://sample/content/modules/mimeDecrypt.jsm").SampleMimeDecrypt;
    const SampleMimeEncrypt = Cu.import("chrome://sample/content/modules/mimeEncrypt.jsm").SampleMimeEncrypt;
    const SampleOverlays = Cu.import("chrome://sample/content/modules/sampleOverlays.jsm").SampleOverlays;
    const Services = Cu.import("resource://gre/modules/Services.jsm").Services;

    if (reason === APP_SHUTDOWN) return;

    shutdownModule(SampleMimeDecrypt, reason);
    shutdownModule(SampleMimeEncrypt, reason);
    shutdownModule(SampleOverlays, reason);
    unloadModules(["mimeDecrypt.jsm", "mimeEncrypt.jsm"]);

    // HACK WARNING: The Addon Manager does not properly clear all addon related caches on update;
    //               in order to fully update images and locales, their caches need clearing here
    Services.obs.notifyObservers(null, "chrome-flush-caches", null);
  }
  catch (ex) {
    logException(ex);
  }
}

/**
 * Perform shutdown of a module
 */
function shutdownModule(module, reason) {
  try {
    module.shutdown(reason);
  }
  catch (ex) {}
}


/**
 * Unload all Enigmail modules that were potentially loaded
 */
function unloadModules(aModules) {
  for (let mod of aModules) {
    try {
      // cannot unload filtersWrapper as you can't unregister filters in TB
      Cu.unload("chrome://sample/content/modules/" + mod);
    }
    catch (ex) {}
  }
}

function logException(exc) {
  try {
    const Services = Cu.import("resource://gre/modules/Services.jsm").Services;
    Services.console.logStringMessage(exc.toString() + "\n" + exc.stack);
  }
  catch (x) {}
}
