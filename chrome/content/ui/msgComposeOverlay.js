/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/*global Components: false, gMsgCompose: false */

"use strict";

Components.utils.import("resource://gre/modules/Services.jsm"); /* global Services: false */

var SampleCompose = {
  DEBUG: function(str) {
    try {
      Services.console.logStringMessage(str);
    }
    catch (x) {}
  },

  createSecurityInfo: function(val) {

    let compSec = Components.classes["@enigmail.net/example/compose-encrypted;1"].
    createInstance(Components.interfaces.nsIMsgComposeSecure);

    compSec = compSec.wrappedJSObject;
    compSec.init(val);
    return compSec;
  },

  /**
   * Handle the 'compose-send-message' event from TB
   */
  sendMessageListener: function(event) {
    this.DEBUG("msgComposeOverlay.js: SampleCompose.sendMessageListener()\n");

    if (gMsgCompose.compFields) {
      if ("securityInfo" in gMsgCompose.compFields) {
        // TB < 64
        gMsgCompose.compFields.securityInfo = this.createSecurityInfo(4711);
      }
      else {
        gMsgCompose.compFields.composeSecure = this.createSecurityInfo(4711);
      }
    }
  }
};

// Listen to message sending event
window.addEventListener('compose-send-message',
  SampleCompose.sendMessageListener.bind(SampleCompose),
  true);
