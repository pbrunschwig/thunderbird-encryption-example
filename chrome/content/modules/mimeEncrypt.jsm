/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/*global Components: false */

"use strict";

/**
 *  Module for creating PGP/MIME signed and/or encrypted messages
 *  implemented as XPCOM component
 */

var EXPORTED_SYMBOLS = ["SampleMimeEncrypt"];

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cr = Components.results;
const Cu = Components.utils;
const Cm = Components.manager;
Cm.QueryInterface(Ci.nsIComponentRegistrar);

const Services = Cu.import("resource://gre/modules/Services.jsm").Services;
const XPCOMUtils = Cu.import("resource://gre/modules/XPCOMUtils.jsm").XPCOMUtils;

// our own contract IDs. Change to both for your own Addon
const MY_MIME_ENCRYPT_CONTRACTID = "@example.net/example/compose-encrypted;1"; // any *unique* string will do
const MY_MIME_JS_ENCRYPT_CID = Components.ID("{c3196a76-1e69-41fe-b336-fc71d1b1a93a}"); // a random UUID

// S/MIME API (required for TB < 64)
const SMIME_JS_ENCRYPT_CONTRACTID = "@mozilla.org/messengercompose/composesecure;1";
const kSmimeComposeSecureCID = "{dd753201-9a23-4e08-957f-b3616bf7e012}";

var gDebugLogLevel = 0;

function MimeEncrypt() {
  this.wrappedJSObject = this;
  this.signMessage = false;
  this.requireEncryptMessage = false;
  this.sampleValue = null;
}

MimeEncrypt.prototype = {
  classDescription: "Sample JS Encryption Handler",
  classID: MY_MIME_JS_ENCRYPT_CID,
  get contractID() {
    if (Components.classesByID[kSmimeComposeSecureCID]) {
      // hack needed for TB < 64: we overwrite the S/MIME encryption handler
      return SMIME_JS_ENCRYPT_CONTRACTID;
    }
    else {
      return MY_MIME_ENCRYPT_CONTRACTID;
    }
  },

  QueryInterface: XPCOMUtils.generateQI([
    "nsIMsgComposeSecure",
    "nsIMsgSMIMECompFields" // Only required for TB < 64
  ]),

  // private variables

  inStream: Cc["@mozilla.org/scriptableinputstream;1"].createInstance(Ci.nsIScriptableInputStream),
  msgCompFields: null,
  msgIdentity: null,
  smimeCompose: null,

  sampleValue: null, // an example value to signal to mimeEncrypt to encrypt the message

  init: function(exampleValue) {
    this.sampleValue = exampleValue;
  },

  // nsIMsgComposeSecure implementation

  signMessage: false,
  requireEncryptMessage: false,

  /**
   * Determine if encryption is required or not
   * (nsIMsgComposeSecure interface)
   *
   * @param {nsIMsgIdentity}   msgIdentity:   the sender's identity
   * @param {nsIMsgCompFields} msgCompFields: the msgCompFields object of the composer window
   *
   * @return {Boolean}:  true if the message should be encrypted, false otherwiese
   */
  requiresCryptoEncapsulation: function(msgIdentity, msgCompFields) {
    DEBUG("mimeEncrypt.jsm: requiresCryptoEncapsulation()\n");

    this.msgCompFields = msgCompFields;
    this.msgIdentity = msgIdentity;
    this.outBuffer = "";
    this.outStringStream = Cc["@mozilla.org/io/string-input-stream;1"].createInstance(Ci.nsIStringInputStream);

    // 1st determine if S/MIME shoud be used

    // Add your code here to determine if the message should be decrypted
    // return true if yes and false otherwise
    if ("securityInfo" in msgCompFields) {
      try {
        // TB < 64 holds the relevant data in securityInfo.
        let secInfo = msgCompFields.securityInfo.wrappedJSObject;
        this.requireEncryptMessage = secInfo.requireEncryptMessage;
        this.signMessage = secInfo.signMessage;
        this.sampleValue = secInfo.sampleValue;
      }
      catch (ex) {
        return false;
      }
    }

    // test if we got our sampleValue from msgComposeOverlay.js
    return this.sampleValue == 4711;
  },

  /**
   * Prepare for encrypting the data (called before we get the message data)
   * (nsIMsgComposeSecure interface)
   *
   * @param {nsIOutputStream}      outStream: the stream that will consume the result of our decryption
   * @param {String}           recipientList: List of recipients, separated by space
   * @param {nsIMsgCompFields} msgCompFields: the msgCompFields object of the composer window
   * @param {nsIMsgIdentity}     msgIdentity: the sender's identity
   * @param {nsIMsgSendReport}    sendReport: report progress to TB
   * @param {Boolean}                isDraft: true if saving draft
   *
   * (no return value)
   */
  beginCryptoEncapsulation: function(outStream, recipientList, msgCompFields, msgIdentity, sendReport, isDraft) {
    DEBUG("mimeEncrypt.jsm: beginCryptoEncapsulation()\n");

    if (this.useSmime) {
      this.smimeCompose.beginCryptoEncapsulation(outStream, recipientList,
        msgCompFields, msgIdentity,
        sendReport, isDraft);
      return;
    }

    if (!outStream) throw Cr.NS_ERROR_NULL_POINTER;

    this.outStream = outStream;
    this.isDraft = isDraft;

    this.writeOut("Content-Type: multipart/thunderbird-sample\r\n\r\n");
  },

  /**
   * Encrypt a block of data (we are getting called for every bit of
   * data that TB sends to us). Most likely the data gets fed line by line
   * (nsIMsgComposeSecure interface)
   *
   * @param {String} buffer: buffer containing the data
   * @param {Number} length: number of bytes
   *
   * (no return value)
   */
  mimeCryptoWriteBlock: function(buffer, length) {
    if (gDebugLogLevel > 4)
      DEBUG(`mimeEncrypt.jsm: mimeCryptoWriteBlock(): ${length}\n`);

    if (this.checkSMime && (!this.smimeCompose))
      throw Cr.NS_ERROR_NOT_INITIALIZED;

    if (this.useSmime) {
      this.smimeCompose.mimeCryptoWriteBlock(buffer, length);
      return;
    }

    // just buffer up the data
    if (length > 0) this.outBuffer += buffer;
  },

  /**
   * we got all data; time to return something to Thunderbird
   * (nsIMsgComposeSecure interface)
   *
   * @param {Boolean}          abort: if true, sending is aborted
   * @param {nsIMsgSendReport} sendReport: report progress to TB
   *
   * (no return value)
   */
  finishCryptoEncapsulation: function(abort, sendReport) {
    DEBUG("mimeEncrypt.jsm: finishCryptoEncapsulation()\n");

    if (this.checkSMime && (!this.smimeCompose))
      throw Cr.NS_ERROR_NOT_INITIALIZED;

    if (this.useSmime) {
      this.smimeCompose.finishCryptoEncapsulation(abort, sendReport);
      return;
    }

    let encryptedData = this.encryptData() + "\r\n";

    this.writeOut(encryptedData);
  },

  encryptData: function() {
    // our encryption function. For the purpose of the example, we just
    // encode everything to BASE64

    return btoa(this.outBuffer).replace(/(.{72})/g, "$1\r\n");
  },

  writeOut: function(str) {
    this.outStringStream.setData(str, str.length);
    var writeCount = this.outStream.writeFrom(this.outStringStream, str.length);
    if (writeCount < str.length) {
      DEBUG(`mimeEncrypt.jsm: writeOut: wrote ${writeCount} instead of  ${str.length} bytes\n`);
    }
  }
};


/**
 * Factory used to register a component in Thunderbird
 */

class Factory {
  constructor(component) {
    this.component = component;
    this.register();
    Object.freeze(this);
  }

  createInstance(outer, iid) {
    if (outer) {
      throw Cr.NS_ERROR_NO_AGGREGATION;
    }
    return new this.component();
  }

  register() {
    Cm.registerFactory(this.component.prototype.classID,
      this.component.prototype.classDescription,
      this.component.prototype.contractID,
      this);
  }

  unregister() {
    Cm.unregisterFactory(this.component.prototype.classID, this);
  }
}


function DEBUG(str) {
  if (gDebugLogLevel > 0) {
    try {

      Services.console.logStringMessage(str);
    }
    catch (x) {}
  }
}


// Exported API that will register and unregister the class Factory

var SampleMimeEncrypt = {
  startup: function(reason) {
    this.factory = new Factory(MimeEncrypt);
  },

  shutdown: function(reason) {
    if (this.factory) {
      this.factory.unregister();
    }
  }
};
