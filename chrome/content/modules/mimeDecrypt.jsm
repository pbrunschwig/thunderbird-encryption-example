/*global Components: false */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

"use strict";

var EXPORTED_SYMBOLS = ["SampleMimeDecrypt"];

/**
 *  Module for handling PGP/MIME encrypted messages
 *  implemented as an XPCOM object
 */

/*global atob: false */

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cr = Components.results;
const Cu = Components.utils;
const Cm = Components.manager;
Cm.QueryInterface(Ci.nsIComponentRegistrar);


const XPCOMUtils = Cu.import("resource://gre/modules/XPCOMUtils.jsm").XPCOMUtils;
const Services = Cu.import("resource://gre/modules/Services.jsm").Services;

var gDebugLogLevel = 0;

// Registering to Thunderbird happens by creating an object with _this_
// contract ID. Do not change it
const MIME_JS_DECRYPTOR_CONTRACTID = "@mozilla.org/mime/pgp-mime-js-decrypt;1";

// your Contract IDs
const MIME_JS_DECRYPTOR_CID = Components.ID("{478675bb-e58e-4495-9e56-0a3b10c280d7}");

////////////////////////////////////////////////////////////////////
// handler for PGP/MIME encrypted messages
// data is processed from libmime -> nsPgpMimeProxy

function MimeDecryptHandler() {

  DEBUG("mimeDecrypt.jsm: new MimeDecryptHandler()\n");
  this.mimeSvc = null;
  this.dataBuffer = "";
}

MimeDecryptHandler.prototype = {
  classDescription: "Enigmail JS Decryption Handler",
  classID: MIME_JS_DECRYPTOR_CID,
  contractID: MIME_JS_DECRYPTOR_CONTRACTID,
  QueryInterface: XPCOMUtils.generateQI([Ci.nsIStreamListener]),

  inStream: Cc["@mozilla.org/scriptableinputstream;1"].createInstance(Ci.nsIScriptableInputStream),

  // the MIME handler needs to implement the nsIStreamListener API
  onStartRequest: function(request, uri) {
    DEBUG("mimeDecrypt.jsm: onStartRequest()\n");

    this.mimeSvc = request.QueryInterface(Ci.nsIPgpMimeProxy);
  },

  onDataAvailable: function(req, sup, stream, offset, count) {
    this.inStream.init(stream);
    if (count > 0) {
      this.dataBuffer += this.inStream.read(count);
    }
  },

  onStopRequest: function(request, win, status) {

    let decryptedData = this.decryptData();
    this.mimeSvc.outputDecryptedData(decryptedData, decryptedData.length);
  },

  decryptData: function() {
    let msg = atob(this.dataBuffer.replace(/[\r\n]/g, ""));

    // we need to wrap the result into a multipart/mixed message, otherwise
    // some functions like Edit As New or Forward don't work in Thunderbird

    return `Content-Type: multipart/mixed; boundary="example-boundary"\r\n\r\n--example-boundary\r\n${msg}--example-boundary--\r\n`;
  }
};


/**
 * Factory used to register the component in Thunderbird
 */

class Factory {
  constructor(component) {
    this.component = component;
    this.register();
    Object.freeze(this);
  }

  createInstance(outer, iid) {
    if (outer) {
      throw Cr.NS_ERROR_NO_AGGREGATION;
    }
    return new this.component();
  }

  register() {
    Cm.registerFactory(this.component.prototype.classID,
      this.component.prototype.classDescription,
      this.component.prototype.contractID,
      this);
  }

  unregister() {
    Cm.unregisterFactory(this.component.prototype.classID, this);
  }
}


var SampleMimeDecrypt = {
  startup: function(reason) {
    try {
      this.factory = new Factory(MimeDecryptHandler);

      // re-use the PGP/MIME handler for our own purposes
      // only required if you want to decrypt something else than Content-Type: multipart/encrypted
      let reg = Components.manager.QueryInterface(Ci.nsIComponentRegistrar);
      let pgpMimeClass = Components.classes["@mozilla.org/mimecth;1?type=multipart/encrypted"];

      reg.registerFactory(
        pgpMimeClass,
        "Sample Decryption Module",
        "@mozilla.org/mimecth;1?type=multipart/thunderbird-sample",
        null);
    }
    catch (ex) {
      DEBUG(ex.message);
    }
  },

  shutdown: function(reason) {
    if (this.factory) {
      this.factory.unregister();
    }
  }
};


////////////////////////////////////////////////////////////////////
// General-purpose functions, not exported

function DEBUG(str) {
  if (gDebugLogLevel > 0) {
    try {
      Services.console.logStringMessage(str);
    }
    catch (x) {}
  }
}
